package com.nlmkit.korshunov_am.tm.service;

import com.nlmkit.korshunov_am.tm.entity.Project;
import com.nlmkit.korshunov_am.tm.entity.Task;
import com.nlmkit.korshunov_am.tm.exceptions.ProjectNotFoundException;
import com.nlmkit.korshunov_am.tm.exceptions.TaskNotFoundException;
import com.nlmkit.korshunov_am.tm.exceptions.WrongArgumentException;
import com.nlmkit.korshunov_am.tm.repository.ProjectRepository;
import com.nlmkit.korshunov_am.tm.repository.TaskRepostory;

import java.util.Collections;
import java.util.List;

/**
 * Сервис задачи в проекте
 */
public class ProjectTaskService extends AbstractService{
    /**
     * Приватный конструктор по умолчанию
     */
    private ProjectTaskService(){
        super(CommandHistoryService.getInstance());
        this.projectRepository = ProjectRepository.getInstance();
        this.taskRepostory = TaskRepostory.getInstance();
    }
    /**
     * Единственный экземпляр объекта ProjectTaskService
     */
    private static ProjectTaskService instance = null;

    /**
     * Получить единственный экземпляр объекта ProjectTaskService
     * @return единственный экземпляр объекта ProjectTaskService
     */
    public static ProjectTaskService getInstance(){
        if (instance == null){
            instance = new ProjectTaskService();
        }
        return instance;
    }
    /**
     * Репозитарий проектов
     */
    private final ProjectRepository projectRepository;
    /**
     * Репозитарий задач
     */
    private final TaskRepostory taskRepostory;

    /**
     * Получить список задач проекта
     * @param projectId идентификатор проекта
     * @return список задач
     */
    public List<Task> findAllByProjectId(final Long projectId) throws WrongArgumentException{
        if (projectId == null) throw new WrongArgumentException("Не задано ИД проекта");
        return taskRepostory.findAllByProjectId(projectId);
    }

    /**
     * Получить список задач проекта с учетом пользователя
     * @param projectId идентификатор проекта
     * @param userId ид пользователя
     * @return список задач
     */
    public List<Task> findAllByProjectId(final Long projectId,final Long userId) throws WrongArgumentException {
        if (projectId == null)  throw new WrongArgumentException("Не задано ИД проекта");
        return taskRepostory.findAllByProjectId(projectId,userId);
    }

    /**
     * Удалить задачу из проекта
     * @param projectId идентификатор проекта
     * @param taskId идентификатор задачи
     * @return задача
     */
    public Task removeTaskFromProject(final Long projectId, final Long taskId) throws WrongArgumentException, TaskNotFoundException {
        if (projectId == null)  throw new WrongArgumentException("Не задано ИД проекта");
        if (taskId == null) throw new WrongArgumentException("Не задано ИД задачи");
        final Task task = taskRepostory.findByProjectIdAndId(projectId,taskId);
        if (task == null) throw new TaskNotFoundException("ИД",taskId.toString(),projectId);
        task.setProjectId(null);
        return task;
    }

    /**
     * Удалить задачу из проекта с учетом задачи и проекта
     * @param projectId идентификатор проекта
     * @param taskId идентификатор задачи
     * @param userId ид пользователя
     * @return задача
     */
    public Task removeTaskFromProject(final Long projectId, final Long taskId,final Long userId) throws WrongArgumentException,TaskNotFoundException{
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (projectId == null) throw new WrongArgumentException("Не задано ИД проекта");
        if (taskId == null) throw new WrongArgumentException("Не задано ИД задачи");
        final Task task = taskRepostory.findByProjectIdAndId(projectId,taskId,userId);
        if (task == null) throw new TaskNotFoundException("ИД",userId,taskId.toString(),projectId);
        task.setProjectId(null);
        return task;
    }

    /**
     * Добавить задачу к проекту
     * @param projectId идентификатор проекта
     * @param taskId идентификатор задачи
     * @return задача
     */
    public Task addTaskToProject(final Long projectId, final Long taskId) throws TaskNotFoundException, ProjectNotFoundException, WrongArgumentException {
        if (projectId == null) throw new WrongArgumentException("Не задан проект");
        if (taskId == null) throw new WrongArgumentException("Не задана задача");
        final Project project = projectRepository.findById(projectId);
        if (project == null) throw new ProjectNotFoundException("ИД",projectId.toString());
        final Task task = taskRepostory.findById(taskId);
        if (task == null) throw new TaskNotFoundException("ИД",taskId.toString());
        task.setProjectId(projectId);
        return task;
    }

    /**
     * Добавить задачу к проекту с учетом пользователя
     * @param projectId идентификатор проекта
     * @param taskId идентификатор задачи
     * @param userId ид пользователя
     * @return задача
     */
    public Task addTaskToProject(final Long projectId, final Long taskId,final Long userId) throws TaskNotFoundException, ProjectNotFoundException, WrongArgumentException {
        if (projectId == null) throw new WrongArgumentException("Не задан проект");
        if (taskId == null) throw new WrongArgumentException("Не задана задача");
        if (userId == null) throw new WrongArgumentException("Не задана пользователь");
        final Project project = projectRepository.findById(projectId,userId);
        if (project == null) throw new ProjectNotFoundException("ИД",userId,projectId.toString());
        final Task task = taskRepostory.findById(taskId,userId);
        if (task == null) throw new TaskNotFoundException("ИД",userId,taskId.toString());
        task.setProjectId(projectId);
        return task;
    }

}
