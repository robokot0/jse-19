package com.nlmkit.korshunov_am.tm.service;

public class SystemService extends AbstractService  {
    /**
     * Приватный конструктор по умолчанию
     */
    private SystemService(){
        super(CommandHistoryService.getInstance());
    }
    /**
     * Единственный экземпляр объекта AbstractService
     */
    private static SystemService instance = null;

    /**
     * Получить единственный экземпляр объекта AbstractService
     * @return единственный экземпляр объекта AbstractService
     */
    public static SystemService getInstance(){
        if (instance == null){
            instance = new SystemService();
        }
        return instance;
    }

    /**
     * Показать выход
     * @return 0 выполнено
     */
    public int displayExit(){
        System.out.println("Terminate program");
        return 0;
    }

    /**
     * Приглашение при входе в программу
     */
    public void displayWelcome(){
        System.out.println("**** WELCOME TO TASK MANAGER ****");
        System.out.println("Enter command");
    }

    /**
     * Показать версию
     * @return 0 выполнено
     */
    public int displayVersion() {
        System.out.println("1.0.0");
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Показать справку
     * @return 0 выполнено
     */
    public int displayHelp() {
        logger.info("Short command name in brackets");
        logger.info("----Common commands:");
        logger.info("version - Display version (v)");
        logger.info("about - Display developer info (a)");
        logger.info("help - Display list of command (h)");
        logger.info("exit - Terminate console application (e)");
        return 0;
    }

    /**
     * Показать информацию о разработчике
     * @return
     */
    public int displayAbout() {
        logger.info("Andrey Korshunov");
        logger.info("korshunov_am@nlmk.com");
        ShowResult("[OK]");
        return 0;
    }

}
