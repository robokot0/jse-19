package com.nlmkit.korshunov_am.tm.publisher;

import com.nlmkit.korshunov_am.tm.exceptions.MessageException;
import com.nlmkit.korshunov_am.tm.exceptions.WrongArgumentException;
import com.nlmkit.korshunov_am.tm.listener.Listener;
import com.nlmkit.korshunov_am.tm.service.CommandHistoryService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static com.nlmkit.korshunov_am.tm.TerminalConst.EXIT;
import static com.nlmkit.korshunov_am.tm.TerminalConst.SHORT_EXIT;

public class PublisherImpl implements Publisher {
    /**
     * Логгер
     */
    final Logger logger = LogManager.getLogger(this.getClass().getName());
    /**
     * Список Listener которых надо оповещать
     */
    List<Listener> listeners = new ArrayList<>();
    /**
     * Сервис истории комманд
     */
    CommandHistoryService commandHistoryService = CommandHistoryService.getInstance();
    @Override
    public Publisher addListener(Listener listener) {
        if (!listeners.contains(listener)){
            listeners.add(listener);
        }
        return this;
    }

    @Override
    public void deleteListener(Listener listener) {
        listeners.remove(listener);
    }

    @Override
    public void notifyListener(String command) {
        boolean wasListened = false;
        try {
            for (Listener listener : listeners) {
                wasListened = (listener.notify(command) == 0) || wasListened;
            }
            if (!wasListened)
                throw new WrongArgumentException("Unknown command");
        } catch (MessageException e) {
            commandHistoryService.ShowResult("[FAIL] "+e.getMessage());
        }
    }

    @Override
    public void readAndExecuteCommand(){
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!(EXIT.equals(command) || SHORT_EXIT.equals(command))) {
            command = scanner.nextLine();
            commandHistoryService.AddCommandToHistory(command);
            this.notifyListener(command);
        }
    }
}
