package com.nlmkit.korshunov_am.tm.repository;

import com.nlmkit.korshunov_am.tm.entity.Project;
import com.nlmkit.korshunov_am.tm.entity.Task;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Репозитарий проектов
 */
public class ProjectRepository {
    /**
     * Приватный конструктор по умолчанию
     */
    private ProjectRepository(){
    }
    /**
     * Единственный экземпляр объекта ProjectRepository
     */
    private static ProjectRepository instance = null;

    /**
     * Получить единственный экземпляр объекта ProjectRepository
     * @return единственный экземпляр объекта ProjectRepository
     */
    public static ProjectRepository getInstance(){
        if (instance == null){
            instance = new ProjectRepository();
        }
        return instance;
    }

    /**
     * Проекты
     */
    private SortedMap<String,List<Project>> projects = new TreeMap<>();

    /**
     * Создать проект
     * @param name Имя проекта
     * @param userId ид пользователя
     * @return созданный проект
     */
    public Project create(final String name,final Long userId) {
        final Project project = new Project(name,userId);
        List<Project> projectList = projects.get(name);
        if(projectList==null){
            projectList= new ArrayList<>();
            projects.put(name,projectList);
        }
        projectList.add(project);
        return project;
    }

    /**
     * Создать проект
     * @param name Имя
     * @param description Описание
     * @return созданный проект
     */
    public Project create(final String name,final String description,final Long userId) {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        List<Project> projectList = projects.get(name);
        if(projectList==null){
            projectList= new ArrayList<>();
            projects.put(name,projectList);
        }
        projectList.add(project);
        return project;
    }

    /**
     * Изменить проект
     * @param id Идентификатор
     * @param name Имя
     * @param description Описание
     * @return проект
     */
    public Project update(final Long id,final String name,final String description,final Long userId) {
        final Project project = findById(id);
        project.setId(id);
        if(!name.equals(project.getName())){
            List<Project> projectListold = projects.get(project.getName());
            if(projectListold!=null) projectListold.remove(project);
            List<Project> projectListNew = projects.get(name);
            if(projectListNew==null) {
                projectListNew= new ArrayList<>();
                projects.put(name,projectListNew);
            }
            projectListNew.add(project);
        }
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        return project;
    }

    /**
     * Очистить список проектов
     */
    public void clear() {
        projects.clear();
    }

    /**
     * Очистить список проектов пользователя
     */
    public void clear(final Long userId) {
        projects.forEach((projectName,projectList) -> {
            projectList.removeIf(project -> project.getUserId().equals(userId));
        } );
    }

    /**
     * Поиск проекта по индексу
     * @param index Индекс
     * @return проект если не найдено null
     */
    public Project findByIndex(final int index){
        int currentIndex = 0;
        for (Map.Entry<String, List<Project>> entry : projects.entrySet()) {
            List<Project> projectList = entry.getValue();
            if (index >= currentIndex + projectList.size())
                currentIndex += projectList.size();
            else
                return projectList.get(index-currentIndex);
        }
        return null;
    }

    /**
     * Поиск проекта по индексу и пользователю
     * @param index Индекс
     * @param userId ид пользователя
     * @return проект если не найдено null
     */
    public Project findByIndex(final int index,final Long userId){
        if (userId==null) return  null;
        Project project=findByIndex(index);
        if (project==null) return  null;
        if (!userId.equals(project.getUserId())) return  null;
        return  project;
    }

    /**
     * Поиск проекта по имени
     * @param name имя
     * @return проект если не найдено null
     */
    public Project findByName(final String name){
        List<Project> projectList = projects.get(name);
        if (projectList.size()>0) return projectList.get(0);
        return null;
    }

    /**
     * Поиск проекта по имени и пользователю
     * @param name имя
     * @param userId ид пользователя
     * @return проект если не найдено null
     */
    public Project findByName(final String name,final Long userId){
        if (userId==null) return  null;
        List<Project> projectList = projects.get(name);
        for (final Project project: projectList) {
            if (project.getName().equals(name) && project.getUserId().equals(userId)) return project;
        }
        return null;
    }

    /**
     * Поиск проекта по идентификатору
     * @param id идентификатор
     * @return проект если не найдено null
     */
    public Project findById(final Long id){
        for (Map.Entry<String, List<Project>> entry : projects.entrySet()) {
            List<Project> projectList = entry.getValue();
            for (final Project project: projectList)
                if (project.getId().equals(id)) return project;
        }
        return null;
    }

    /**
     * Поиск проекта по идентификатору и пользователю
     * @param id идентификатор
     * @param userId ид пользователя
     * @return проект если не найдено null
     */
    public Project findById(final Long id,final Long userId){
        if (userId==null) return  null;
        final Project project = findById(id);
        if (project.getUserId().equals(userId)) return project;
        return null;
    }

    /**
     * Удалить проект по индексу
     * @param index индекс
     * @return удаленный проект если не найдено null
     */
    public Project removeByIndex(final int index){
        final Project project = findByIndex(index);
        if (project == null) return null;
        List<Project> projectList = projects.get(project.getName());
        projectList.remove(project);
        return project;
    }

    /**
     * Удалить проект по индексу и пользователю
     * @param index индекс
     * @param userId ид пользователя
     * @return удаленный проект если не найдено null
     */
    public Project removeByIndex(final int index,final Long userId){
        if (userId==null) return  null;
        final Project project = findByIndex(index,userId);
        if (project == null) return null;
        List<Project> projectList = projects.get(project.getName());
        projectList.remove(project);
        return project;
    }

    /**
     * Удалить проект по идентификатору
     * @param id  идентификатор
     * @return удаленный проект если не найдено null
     */
    public Project removeById(final Long id){
        final Project project = findById(id);
        if (project == null) return null;
        List<Project> projectList = projects.get(project.getName());
        projectList.remove(project);
        return project;
    }

    /**
     * Удалить проект по идентификатору и пользователю
     * @param id  идентификатор
     * @param userId ид пользователя
     * @return удаленный проект если не найдено null
     */
    public Project removeById(final Long id,final Long userId){
        if (userId==null) return  null;
        final Project project = findById(id,userId);
        if (project == null) return null;
        List<Project> projectList = projects.get(project.getName());
        projectList.remove(project);
        return project;
    }

    /**
     * Удалить проект по имени
     * @param name имя
     * @return удаленный проект если не найдено null
     */
    public Project removeByName(final String name){
        final Project project = findByName(name);
        if (project == null) return null;
        List<Project> projectList = projects.get(project.getName());
        projectList.remove(project);
        return project;
    }

    /**
     * Удалить проект по имени  и пользователю
     * @param name имя
     * @param userId ид пользователя
     * @return удаленный проект если не найдено null
     */
    public Project removeByName(final String name,final Long userId){
        if (userId==null) return  null;
        final Project project = findByName(name,userId);
        if (project == null) return null;
        List<Project> projectList = projects.get(project.getName());
        projectList.remove(project);
        return project;
    }

    /**
     * Получить список всех проектов
     * @return список проектов
     */
    public List<Project> findAll() {
        final List<Project> result = new ArrayList<>();
        for (Map.Entry<String, List<Project>> entry : projects.entrySet()) {
            List<Project> projectList = entry.getValue();
            for (final Project project: projectList){
                result.add(project);
            }
        }
        return result;
    }

    /**
     * Получить список всех проектов пользователя
     * @param userId ид пользователя
     * @return список проектов
     */
    public List<Project> findAll(final Long userId) {
        final List<Project> result = new ArrayList<>();
        for (Map.Entry<String, List<Project>> entry : projects.entrySet()) {
            List<Project> projectList = entry.getValue();
            for (final Project project: projectList){
                if(project.getUserId().equals(userId)) result.add(project);
            }
        }
        return result;
    }

    /**
     * Получить количество проектов в репозитарии
     * @return количество проектов
     */
    public int size() {
        Integer result=0;
        for (Map.Entry<String, List<Project>> entry : projects.entrySet()) {
            List<Project> projectList = entry.getValue();
            result+=projectList.size();
        }
        return result;
    }
    /**
     * Получить количество проектов  пользователя в репозитарии
     * @param userId ид пользователя
     * @return количество проектов
     */
    public int size(final Long userId) {
        Integer result=0;
        for (Map.Entry<String, List<Project>> entry : projects.entrySet()) {
            List<Project> projectList = entry.getValue();
            for (final Project project: projectList)result++;
        }
        return result;
    }
}
